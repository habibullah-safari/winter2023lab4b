import java.util.Scanner;

public class VirtualPetApp{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		// 1. pack (group of dog)
		Dog[] pack = new Dog[1];
		
		// setting values to the array using a loop 
		// the loop is not needed in this case
		for(int i = 0; i < pack.length; i++){
			
			System.out.println("Input weight");
			double weight = (Double.parseDouble(scan.nextLine()));
			
			System.out.println("Input breed");
			String breed = scan.nextLine();

			System.out.println("Input color");
			String color = scan.nextLine();
			
			// using user input calls the constructor method from dog class
			pack[i] = new Dog(breed, 20, color);
		}
		//
		
		System.out.println(" Enter a new weight ");
		//using setWeight method changes the weight of the last animal in the pack array
		pack[pack.length-1].setWeight(Double.parseDouble(scan.nextLine()));
		System.out.println(pack[pack.length-1].getWeight());
		
		// print all fields for the last animal in the pack array
		for(int index =0; index<pack.length; index++){
			System.out.println(pack[pack.length-1].getWeight());
			System.out.println(pack[pack.length-1].getBreed());
			System.out.println(pack[pack.length-1].getColor());
		}
	}
}
