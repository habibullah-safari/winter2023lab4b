public class Dog{
	private String breed;
	private double weight;
	private String color;
	
	// Set Methods 
	public void setWeight(double newWeight){
		this.weight = newWeight;
	}
		
	//  Get Methods 
	public String getBreed(){
		return this.breed;
	}
	public double getWeight(){
		return this.weight;
	}
	public String getColor(){
		return this.color;
	}
	//Constructor method 
	public Dog( String newBreed, double newWeight, String newColor){
		this.breed = newBreed;
		this.weight = newWeight;
		this.color = newColor;
	}
	
	// Instance Methods
	public void goEat(){
		if(this.weight >= 25){
			System.out.println("Rocket ! Eat less !!!");
		}else{
			System.out.println("Rocket ! Eat more! ");
		}
	}
	
	public void goCrazy(){
		if(this.breed.equals("French Bulldog")){
			System.out.println("Omg zoomies!!! GO CRAZY");
		}else{
			System.out.println("Wow, very calm pet!");
		}
	}
}

